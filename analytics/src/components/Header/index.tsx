import React from 'react'

import style from './style.scss';

export function Header() {
    return (
        <header className={style.header}>
            <h1 className={style.title}>City data</h1>
        </header>
    )
}
