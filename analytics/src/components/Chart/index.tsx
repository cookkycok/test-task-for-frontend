import React from "react";
import {
    CartesianGrid,
    Legend,
    Line,
    LineChart,
    Tooltip,
    XAxis,
    YAxis,
} from 'recharts'

import styles from './style.scss';

export function Chart({cityInfo}) {
    return (
        <div className={styles.chart}>
            <LineChart
                width={730}
                height={250}
                data={cityInfo.map(({year, population, costOfLiving}) => ({
                    year,
                    koef: population / costOfLiving,
                }))}
            >
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="year"/>
                <YAxis/>
                <Tooltip/>
                <Legend/>
                <Line type="monotone" dataKey="koef" stroke="blue"/>
            </LineChart>

        </div>
    )
}
