interface City {
    city: string,
    pathName: string,
}

export interface CityStore {
    cities: City[],
    isLoaded: boolean,
}

export interface CityInfo {
    isLoaded: boolean;
    info: Array<{
        year: number;
        population: number;
        costOfLiving: number;
    }>
}
