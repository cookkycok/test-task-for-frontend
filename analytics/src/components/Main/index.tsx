import React, {useEffect, useState} from 'react'

import {Search} from "../Search";
import {Chart} from "../Chart";
import {loadCity, loadCityInfo, initialCityInfo, initialCityDatas} from "../../utils/store";
import {CityInfo, CityStore} from "./interface";
import style from './style.scss';

export function Main() {
    const [state, setCities] = useState<CityStore>(initialCityDatas);
    const [selectedCity, setCity] = useState('');
    const [cityInfo, setCityInfo] = useState<CityInfo>(initialCityInfo);

    useEffect(loadCity(setCities), []);
    useEffect(loadCityInfo(selectedCity,state.cities, setCityInfo), [selectedCity]);

    if(!state.isLoaded || (selectedCity && !cityInfo.isLoaded)) {
        return <div>Loading ....</div>;
    }

    return (
        <div className={style.main}>
            <Search cities={state.cities} setCity={setCity} defaultValue={selectedCity}/>
            <Chart cityInfo={cityInfo.info}/>
        </div>
    )
}
