import React from 'react'

import style from "./style.scss";

export function Footer() {
    return (
        <footer className={style.footer}>
            <h3 className={style.title}>Footer</h3>
        </footer>
    )
}
