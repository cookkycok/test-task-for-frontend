import React, {useRef, useEffect} from "react";

import styles from './style.scss';

export function Search({cities, setCity, defaultValue}) {
    const inputEl = useRef(null);

    useEffect(() => {
        (window as any).autocomplete(inputEl.current, {
            source: cities.map(({city}) => city),
            debounceTime: 500,
        });
    }, []);

    return (
        <div className={styles.search}>
            <input type="text" id="search-input" placeholder="Search any..." defaultValue={defaultValue} ref={inputEl} onBlur={({target}) => setCity(target.value)} />
        </div>
    )
}
