const pathToData = '../../fixtures';

export const initialCityDatas = {cities: [], isLoaded: false};

export function loadCity(setCities) {
    return () => {
        import(`${pathToData}/cities.json`).then(({default: cities}) => {
            setCities({
                cities,
                isLoaded: true
            });
        })
    }
}

export const initialCityInfo = {info: [], isLoaded: false};

export function loadCityInfo(selectedCity, cities, setCityInfo) {
    return () => {
        const cityName = selectedCity.toLowerCase();
        if (!cities.some(({city}) => city.toLowerCase() === cityName)) {
            return setCityInfo({...initialCityInfo, isLoaded: true});
        }
        setCityInfo(initialCityInfo);
        import(`${pathToData}/data/${cityName}.json`).then(({default: info}) => {
            setCityInfo({info, isLoaded: true});
        });
    }
}
