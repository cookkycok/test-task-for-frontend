interface AutoCompleteOptions extends WrapperClassNames {
    source: string[];
    debounceTime: number;
    before?: () => void;
    after?: () => void;
    inputClassName?: string;
}

export interface AutoComplete {
    (input: HTMLInputElement, options: AutoCompleteOptions): void
}

export interface MakeNode {
    (element: string, options?: any, text?: string): Element
}

export interface WrapperClassNames {
    listClassName?: string;
    wrapperClassName?: string;
}
