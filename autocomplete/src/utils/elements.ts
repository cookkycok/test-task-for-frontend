import {addAttributes, addClass} from "./index";
import style from '../../styles/root.scss';

import {itemScreenReader} from "../data";
import {MakeNode, WrapperClassNames} from "../interface";

export function removeChildren(node: Element): void {
    let child: ChildNode | null;
    while(child = node.firstChild) {
        node.removeChild(child);
    }
}

const makeNode: MakeNode = (element, {classNames} = {}, text = '') => {
    const node = document.createElement(element);
    addClass(node, classNames);
    node.textContent = text;
    return node;
};

function appendChild(parent: Element, nodes: Element[]): void {
    const fragment = document.createDocumentFragment();
    fragment.append(...nodes);
    parent.appendChild(fragment);
}

export function createNodesByResult(result: string[], list: Element): void {
    appendChild(list, result.map((text) => {
        const li = makeNode('li', {classNames: style.item}, text);
        addAttributes(li, itemScreenReader);
        return li;
    }));
}

export function initWrapper(inputNode: Element, className?: string): Element {
    const wrapper = makeNode('div', {classNames: className || style.wrapper});
    (inputNode.parentElement as HTMLElement).appendChild(wrapper);
    wrapper.appendChild(inputNode);
    return wrapper;
}

export function createListElement(className?: string): Element {
    return makeNode('ul', {classNames: className || style.list})
}

export function initElements(inputNode: Element, {listClassName, wrapperClassName}: WrapperClassNames): Element[] {
    const wrapper = initWrapper(inputNode, wrapperClassName);
    const list = createListElement(listClassName);
    wrapper.appendChild(list);
    return [wrapper, list];
}
