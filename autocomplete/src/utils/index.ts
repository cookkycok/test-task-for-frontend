export function debouncer(callFunction: (...args: any[]) => any, debounceTime, callBefore?: () => void) {
    let timer;
    return (...args: any[]) => {
        if(callBefore) {
            callBefore();
        }
        clearTimeout(timer);
        timer = setTimeout(callFunction.bind(callFunction, ...args), debounceTime);
    }
}

export function prepareSource(search: string[]) {
    return search.map((value) => value.toLowerCase());
}

export function filterSearch(value: string) {
    return (state) => state.indexOf(value.toLowerCase()) !== -1;
}

export function addClass(node: Element, className: string) {
    node.classList.add(className);
}

export function removeClass(node: Element, className: string | string[]) {
    if(!Array.isArray(className)) {
        className = [className];
    }
    node.classList.remove(...className);
}

export function noop() {}

export function addAttributes(element: Element, attributes: Array<string[]>) {
    attributes.forEach(([attribute, value]) => element.setAttribute(attribute, value));
}
