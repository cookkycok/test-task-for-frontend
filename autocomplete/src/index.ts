import {addAttributes, addClass, debouncer, filterSearch, noop, prepareSource, removeClass} from "./utils";

import {createNodesByResult, initElements, removeChildren} from "./utils/elements";
import {inputScreenReader, listScreenReader} from "./data";
import {AutoComplete} from "./interface";
import style from '../styles/root.scss';

function handlerSearch(search: string[], nodeResult: Element, after: (...args: any[]) => void) {
    return ({target}: Event) => {
        removeChildren(nodeResult);
        removeClass(nodeResult, [style.loading, style.none]);

        const {value} = target as HTMLInputElement;
        const result = !!value ? search.filter(filterSearch(value)) : [];

        after(result);
        if(value && !result.length) {
            return addClass(nodeResult, style.none);
        }

        createNodesByResult(result, nodeResult);
    }
}

const autocomplete: AutoComplete = (input: HTMLInputElement, {source = [], debounceTime = 0, before = noop, after = noop, inputClassName, listClassName, wrapperClassName}) => {
    const [, list] = initElements(input, {listClassName, wrapperClassName});
    addClass(input, inputClassName || style.input);

    input.addEventListener('input', debouncer(
        handlerSearch(
            prepareSource(source),
            list,
            after,
        ),
        debounceTime,
        () => {
            addClass(list, style.loading);
            before();
        }
    ));

    addAttributes(input, inputScreenReader);
    addAttributes(list, listScreenReader);

    input.addEventListener('focus', () => removeClass(list, style.hide));
    input.addEventListener('blur', () => addClass(list, style.hide));
    list.addEventListener('mousedown', ({target}: Event) => {
        input.value = (target as HTMLElement).textContent || '';
    })
};

(window as any).autocomplete = autocomplete;
