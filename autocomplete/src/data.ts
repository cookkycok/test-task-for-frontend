export const inputScreenReader = [
    ['aria-describedby', 'initInstr'],
    ['aria-owns', 'results'],
    ['aria-expanded', 'false'],
    ['aria-autocomplete', 'both'],
    ['aria-activedescendant', ''],
];

export const listScreenReader = [
    ['id', 'results'],
    ['role', 'listbox'],
];

export const itemScreenReader = [
    ['aria-selected', 'false'],
    ['role', 'option'],
];
